------------
--TRIGGERS--
------------

CREATE TABLE log_table (
    user_id      VARCHAR2(30),
    logon_date   DATE
);     --Crea tabla

--trigger
CREATE OR REPLACE TRIGGER logon_trigg AFTER LOGON ON DATABASE BEGIN
    INSERT INTO log_table (
        user_id,
        logon_date
    ) VALUES (
        user,
        sysdate
    );
END;