--Ejemplos de Bloques An�nimos--

DECLARE
    v_first_name   VARCHAR2(25);
    v_last_name    VARCHAR2(25);
BEGIN
    SELECT
        first_name,
        last_name
    INTO
        v_first_name, --variables
        v_last_name
    FROM
        copy_employees
    WHERE
        
        last_name = 'King';

    dbms_output.put_line('The employee of the month is: '
                         || v_first_name
                         || ' '
                         || v_last_name
                         || '.');

EXCEPTION
    WHEN too_many_rows THEN         --si hay mas de un empleado Oswald cacha la excepci�n e imprime lo siguiente en consola
        dbms_output.put_line('Your select statement retrieved  
        multiple rows. Consider using a cursor or changing
        the search criteria.');
END;




--Ejemplos de Bloques An�nimos--
-- 2 excepciones--

DECLARE
    v_first_name   VARCHAR2(25);
    v_last_name    VARCHAR2(25);
BEGIN
    SELECT
        first_name,
        last_name
    INTO
        v_first_name, --variables
        v_last_name
    FROM
        copy_employees
    WHERE
        
        last_name = 'Oswald';

    dbms_output.put_line('The employee of the month is: '
                         || v_first_name
                         || ' '
                         || v_last_name
                         || '.');

EXCEPTION
    WHEN too_many_rows THEN         --si hay mas de un empleado con el mismo apellido cacha la excepci�n e imprime lo siguiente en consola
        dbms_output.put_line('Your select statement retrieved  
        multiple rows. Consider using a cursor or changing
        the search criteria.');
    
   WHEN no_data_found THEN
    dbms_output.put_line('No se encontro ning�n usuario.');   --Si no se encuentra el usuario entra en �sta excepci�n 
END;




--Ejemplos de bloques de c�digo de subprograma--

--Bloque de c�digo para crear un procedimiento llamado PRINT_DATE--
CREATE OR REPLACE PROCEDURE print_date IS   --define un procedimiento
    v_date VARCHAR2(30);
BEGIN
    SELECT
        to_char(sysdate +1, 'Mon DD, YYYY')--Agrega un d�a a la fecha actual
    INTO v_date
    FROM
        dual;

    dbms_output.put_line(v_date);
END;

--definido el procedimiento lo ejecuta--

BEGIN
    PRINT_DATE;   --Imprime fecha en el formato seleccionado el procedimiento
END;




-- Segudo Ejemplo de bloques de c�digo de subprograma--
--Crea una funci�n--

CREATE OR REPLACE FUNCTION tomorrow (p_today IN DATE) -- fecha recibida 
    RETURN DATE IS
    v_tomorrow DATE; --define variable en la que guarda
BEGIN
    SELECT
        p_today + 1 --Agrega un d�a a la fecha recibida
    INTO v_tomorrow  --Guarda la fecha  
    FROM
        dual;

    RETURN v_tomorrow;
END;

--llamar a la funci�n utilizando una instrucci�n SQL o un bloque PL / SQL

 --SQL
SELECT TOMORROW(SYSDATE) AS "Tomorrow's Date"  
FROM DUAL;

 --PL/SQL
BEGIN
DBMS_OUTPUT.PUT_LINE(TOMORROW(SYSDATE));
END;




--Declarando e inicializando variables

DECLARE
    v_myname VARCHAR2(20);
BEGIN
    dbms_output.put_line('My name is: ' || v_myname);
    v_myname := 'John';
    dbms_output.put_line('My name is: ' || v_myname);
END;

--Segundo ejemplo

DECLARE
    v_date VARCHAR2(30);
BEGIN
    SELECT
        to_char(sysdate)
    INTO v_date
    FROM
        dual;

    dbms_output.put_line(v_date);
END;


-- Crear funci�n

CREATE FUNCTION num_characters (p_string IN VARCHAR2)
RETURN INTEGER is
    v_num_characters INTEGER;
BEGIN
    SELECT
        length(p_string)
    INTO v_num_characters
    FROM
        dual;

    RETURN v_num_characters;
END;

--Ejecutar funci�n

DECLARE
    v_length_of_string INTEGER;
BEGIN
    v_length_of_string := num_characters('Oracle    
Corporation');
    dbms_output.put_line(v_length_of_string); --En la llamada a la funci�n num_characters, el valor devuelto por la funci�n se almacenar� en la variable v_length_of_string.
END;




--Ejemplo alcance Variables

DECLARE
    v_father_name     VARCHAR2(20) := 'Patrick';
    v_date_of_birth   DATE := '20-Abril-1972';
BEGIN
    DECLARE
        v_child_name VARCHAR2(20) := 'Mike';
    BEGIN
        dbms_output.put_line('Father''s Name: ' || v_father_name);
        dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
        dbms_output.put_line('Child''s Name: ' || v_child_name);
    END;

    dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
END;




--Calificaci�n de un identificador (Qualifying an Identifier)

<<outer>> --Etiqueta para imprimir bloques externos
DECLARE
    v_father_name     VARCHAR2(20) := 'Patrick';
    v_date_of_birth   DATE := '20-Abril-1972';
BEGIN
    DECLARE
        v_child_name      VARCHAR2(20) := 'Mike';
        v_date_of_birth   DATE := '12-Diciembre-2002';
    BEGIN
        dbms_output.put_line('Father''s Name: ' || v_father_name);
        dbms_output.put_line('Date of Birth: ' || outer.v_date_of_birth); --Bloque externo <outer>
        dbms_output.put_line('Child''s Name: ' || v_child_name);
        dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
    END;
END;