----------
--CURSOR--
----------


DECLARE
    CURSOR cur_depts IS
    SELECT
        department_id,
        department_name
    FROM
        departments;

    v_department_id     departments.department_id%TYPE;   --Define variable con el tipo de columna deparment_id
    v_department_name   departments.department_name%TYPE; ----Define variable con el tipo de columna deparment_name
BEGIN
    OPEN cur_depts;             --Abre el cursor
    LOOP                        --Itera el cursor
        FETCH cur_depts INTO      --INTO guarda el resultado de la iteración en las variables que se definieron
            v_department_id,
            v_department_name;
        EXIT WHEN cur_depts%notfound;      --Condición para terminar la iteración
        dbms_output.put_line(v_department_id
                             || ' '
                             || v_department_name);    --Impresión de variables
    END LOOP;

    CLOSE cur_depts;               --Cierra el cursor
END;


--

DECLARE
    CURSOR cur_depts IS
    SELECT
        department_id,
        department_name,
        location_id
    FROM
        departments;

    v_department_id     departments.department_id%TYPE;
    v_department_name   departments.department_name%TYPE;
    v_location_id       departments.location_id%TYPE;
BEGIN
    OPEN cur_depts;
    LOOP
        FETCH cur_depts INTO
            v_department_id,
            v_department_name,
            v_location_id;
        EXIT WHEN cur_depts%notfound;

----------------------
--CURSOR AND RECORDS--
----------------------

DECLARE
    CURSOR cur_emps IS
    SELECT
        *
    FROM
        employees
    WHERE
        department_id = 30;

    v_emp_record cur_emps%rowtype;  --Utiliza ROWNTYPE para regresar todos los campos de la tabla
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_record;
        EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_emp_record.employee_id
                             || ' - '
                             || v_emp_record.last_name);
    END LOOP;

    CLOSE cur_emps;
END;



--------------------------------
--VARIABLES GLOBALES Y LOCALES--
--------------------------------
DECLARE
    v_outer_variable VARCHAR2(20):='GLOBAL VARIABLE';
BEGIN
    DECLARE
        v_inner_variable VARCHAR2(20):='LOCAL VARIABLE';
    BEGIN
        DBMS_OUTPUT.PUT_LINE(v_inner_variable);
        DBMS_OUTPUT.PUT_LINE(v_outer_variable);
    END;
    DBMS_OUTPUT.PUT_LINE(v_outer_variable);
END;


--Llamar variables externas--
<<outer>>
DECLARE
    v_father_name VARCHAR2(20):='Patrick';
    v_date_of_birth DATE:='20-04-1972';
BEGIN
    DECLARE
        v_child_name VARCHAR2(20):='Mike';
        v_date_of_birth DATE:='12-12-2002';
    BEGIN
        DBMS_OUTPUT.PUT_LINE('Father''s Name: ' || v_father_name);
        DBMS_OUTPUT.PUT_LINE('Date of Birth: ' || outer.v_date_of_birth);
        DBMS_OUTPUT.PUT_LINE('Child''s Name: ' || v_child_name);
        DBMS_OUTPUT.PUT_LINE('Date of Birth: ' || v_date_of_birth);
    END;
END;



---------------------
--Cursores anidados--
---------------------

declare 
    CURSOR cur_dept IS
        SELECT department_id, department_name
            FROM departments
            ORDER BY department_name;
    CURSOR cur_emp (p_deptid NUMBER) IS
        SELECT first_name, last_name
            FROM employees
            WHERE department_id = p_deptid
            ORDER BY last_name;
    v_deptrec   cur_dept%rowtype;
    v_emprec    cur_emp%rowtype;
BEGIN
    OPEN cur_dept;
    LOOP
        FETCH cur_dept INTO v_deptrec; --Muestra el departamento y sus trabajadores 
        EXIT WHEN cur_dept%notfound;
        dbms_output.put_line(v_deptrec.department_name);
        OPEN cur_emp(v_deptrec.department_id);
        LOOP
            FETCH cur_emp INTO v_emprec;
            EXIT WHEN cur_emp%notfound;
            dbms_output.put_line(v_emprec.last_name
                                 || ' '
                                 || v_emprec.first_name);
        END LOOP;
        CLOSE cur_emp;
    END LOOP;
    CLOSE cur_dept;
END;

-- hace lo mismo pero con for loops 
DECLARE
    CURSOR cur_dept IS SELECT * FROM departments;
    CURSOR cur_emp (p_dept_id NUMBER) IS
        SELECT * FROM employees WHERE department_id = p_dept_id
        FOR UPDATE NOWAIT;
BEGIN
    FOR v_deptrec IN cur_dept LOOP
        DBMS_OUTPUT.PUT_LINE(v_deptrec.department_name||':');
        FOR v_emprec IN cur_emp (v_deptrec.department_id) LOOP
            DBMS_OUTPUT.PUT_LINE(v_emprec.last_name);
            IF v_deptrec.location_id = 1700 AND v_emprec.salary < 10000 --y actualiza el salario de esta condición
                THEN UPDATE employees
                        SET salary = salary * 1.1
                        WHERE CURRENT OF cur_emp;
            END IF;
        END LOOP;
    END LOOP;
END;